<?php

namespace Drupal\open_demographics\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'gender' widget.
 *
 * @FieldWidget(
 *   id = "gender",
 *   module = "open_demographics",
 *   label = @Translation("Gender"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class Gender extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['#type'] = 'details';
    $element['#open'] = TRUE;

    $element['gender_transgender'] = [
      '#type' => 'radios',
      '#title' => $this->t('Do you consider yourself to be transgender?'),
      '#options' => [
        '' => $this->t('- Select -'),
        'yes' => $this->t('Yes'),
        'no' => $this->t('No'),
        'questioning' => $this->t('Questioning'),
      ],
      '#default_value' => !empty($items[$delta]->value['gender_transgender']) ? $items[$delta]->value['gender_transgender'] : NULL,
      '#required' => $element['#required'],
    ];

    $element['gender_non_conforming'] = [
      '#type' => 'radios',
      '#title' => $this->t('Do you consider yourself to be gender non-conforming, gender diverse, gender variant, or gender expansive?'),
      '#options' => [
        'yes' => $this->t('Yes'),
        'no' => $this->t('No'),
        'questioning' => $this->t('Questioning'),
      ],
      '#default_value' => !empty($items[$delta]->value['gender_non_conforming']) ? $items[$delta]->value['gender_non_conforming'] : NULL,
      '#required' => $element['#required'],
    ];

    $element['gender_intersex'] = [
      '#type' => 'radios',
      '#title' => $this->t('Are you intersex?'),
      '#options' => [
        'yes' => $this->t('Yes'),
        'no' => $this->t('No'),
        'questioning' => $this->t('Questioning'),
      ],
      '#default_value' => !empty($items[$delta]->value['gender_intersex']) ? $items[$delta]->value['gender_intersex'] : NULL,
      '#required' => $element['#required'],
    ];

    $element['gender_spectrum'] = [
      '#type' => 'select_or_other_buttons',
      '#title' => $this->t('Where do you identify on the gender spectrum (check all that apply)?'),
      '#options' => [
        'woman' => 'Woman',
        'demi-girl' => 'Demi-girl',
        'man' => 'Man',
        'demi-boy' => 'Demi-boy',
      ],
      '#multiple' => TRUE,
      'select' => [
        '#type' => 'checkboxes',
        '#default_value' => !empty($items[$delta]->value['gender_spectrum']) ? explode(',', $items[$delta]->value['gender_spectrum']) : NULL,
      ],
      '#required' => $element['#required'],
    ];

    return $element;
  }

}
